<?php

namespace App\Events;

use App\Models\Blog;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BlogCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $blogs;

    public function __construct(Blog $blogs)
    {
        $this->blogs = $blogs;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('blogs-created');
    }
}
