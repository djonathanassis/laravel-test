<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogStoreRequest;
use App\Http\Resources\BlogsResource;
use App\Models\Blog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class BlogsController extends Controller
{

    function __construct()
    {
        $this->middleware('user.block.name', ['only' => ['store']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlogStoreRequest $request
     */
    public function store(BlogStoreRequest $request)
    {
        $input = $request->validated();

        $blogs = $request->user()->blogs()->create($input);

        return (new BlogsResource($blogs))
            ->response()
            ->setStatusCode(ResponseAlias::HTTP_CREATED);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse|object
     */
    public function update(Request $request, $id)
    {
        $blogs = Blog::query();

        if (!$blogs->find($id))
        {
            return response()->json('Registro não encontrado', 422);
        }

        $blogs->update($request->all());

        return response()->json('Atualizado com sucesso!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Blog $blog
     * @return Response
     */
    public function delete(Blog $blogs): Response
    {
        if (!Gate::allows('delete-blogs', $blogs)) abort(403);

        $blogs->delete();

        return response()->noContent();

    }
}
