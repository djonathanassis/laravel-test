<?php

namespace App\Listeners;

use App\Events\BlogCreated;
use App\Mail\BlogCopy;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendBlogEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogCreated  $event
     * @return void
     */
    public function handle(BlogCreated $event)
    {
      Mail::to($event->blogs->user->email)->send(new BlogCopy($event->blogs));
    }
}
