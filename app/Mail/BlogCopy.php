<?php

namespace App\Mail;

use App\Models\Blog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class BlogCopy extends Mailable
{
    use Queueable, SerializesModels;

    public $blog;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      dd("OK");
      return $this->view('emails.new_daily_log')
      ->subject('Daily Log Created with Success !!!')
      ->with([
        'dailyLog' => $this->dailyLog
      ]);
    }
}
