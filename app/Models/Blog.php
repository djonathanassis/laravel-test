<?php

namespace App\Models;

use App\Events\BlogCreated;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'blogs';

    protected $fillable = [
        'name',
        'domain',
        'owner_id',
    ];

    protected $dates = [
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $events = [
        'created' => BlogCreated::class
    ];

    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }


}
