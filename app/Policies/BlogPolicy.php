<?php

namespace App\Policies;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the DailyLog.
    */
    public function delete(User $user, Blog $blogs) : mixed
    {
       return $user->id === $blogs->user_id;
    }
}
