<?php

use App\Http\Controllers\BlogsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware('auth')->group(function () {

    Route::prefix('blogs')->group(function () {
        Route::post('', [BlogsController::class, 'store'])->name('blogs.store');
        Route::put('{blogs}', [BlogsController::class, 'update'])->name('blogs.update');
        Route::delete('{blogs}', [BlogsController::class, 'delete'])->name('blogs.delete');
    });
});

//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//    return Inertia\Inertia::render('Dashboard');
//})->name('dashboard');
